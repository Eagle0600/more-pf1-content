const Extensions = {
	getControlledActors,
	selectAndUseAbility,
	polymorphActor,
}

/**
 * Activate or deactivate a polymorph form.
 * @param formItem - the item that contains the form information
 * @param activating - true if the form is activating, false if it is deactivating
 */
async function polymorphActor(actor, formItem, activating) {
	if (!actor) return;
	const updateData = {}, tokenData = {}, itemData = [];
	var actorImage, tokenImage, actorSize, tokenSize, oldForm, formName = "";
	if (activating) {
		// Change to alternate form.
		formName = formItem.getItemDictionaryFlag("polymorphFormName") || formItem.name;
		actorImage = formItem.getItemDictionaryFlag("polymorphActorImage");
		tokenImage = formItem.getItemDictionaryFlag("polymorphTokenImage");
		actorSize = formItem.getItemDictionaryFlag("polymorphActorSize");
		tokenSize = Number(formItem.getItemDictionaryFlag("polymorphTokenSize"));
		updateData["flags.pf1MoreJpw.polymorph.formId"] = formItem.id;
		
		const fromOriginal = !actor.flags?.pf1MoreJpw?.polymorph?.isPolymorphed;
		if (fromOriginal) {
			// Store original form data.
			updateData["flags.pf1MoreJpw.polymorph.original.actorImage"] = actor.img;
			updateData["flags.pf1MoreJpw.polymorph.original.actorSize"] = actor.system.traits.size;
			updateData["flags.pf1MoreJpw.polymorph.original.tokenImage"] = actor.token.texture.src;
			updateData["flags.pf1MoreJpw.polymorph.original.tokenWidth"] = actor.token.width;
			updateData["flags.pf1MoreJpw.polymorph.original.tokenHeight"] = actor.token.height;
			updateData["flags.pf1MoreJpw.polymorph.isPolymorphed"] = true;
			updateData["flags.pf1MoreJpw.polymorph.isPolymorphed"] = true;
		} else {
			oldForm = actor.flags?.pf1MoreJpw?.polymorph?.formId;
		}
	} else {
		if (actor.flags?.pf1MoreJpw?.polymorph?.formId != formItem.id)
			return;
		// Revert to original form.
		const original = actor.flags?.pf1MoreJpw?.polymorph?.original;
		if (original) {
			actorImage = original.actorImage;
			actorSize = original.actorSize;
			tokenImage = original.tokenImage;
			tokenSize = original.tokenWidth;
		}
		updateData["flags.pf1MoreJpw.polymorph.isPolymorphed"] = false;
		updateData["flags.pf1MoreJpw.polymorph.formId"] = "";
	}
		
	// Enabled and disable abilities.
	if (formItem.hasItemBooleanFlag("polymorphChangesAbilities")) {
		actor.items.forEach(i => {
			let formDependant = i.getItemDictionaryFlag("formDependant");
			if (formDependant == undefined)
				return;
			let forms = formDependant.split(";");
			var active = forms.includes(formName);
			if (i.type == "attack") {
				// Attacks cannot be disabled, so we simply remove them from the quickbar.
				itemData.push({_id: i.id, "system.showInQuickbar": active});
			} else if (i.system.equipped != undefined) {
				itemData.push({_id: i.id, "system.equipped": active});
			} else {
				itemData.push({_id: i.id, "system.disabled": !active});
			}
		});
		actor.updateEmbeddedDocuments("Item", itemData);
	}

	if (actorImage)
		updateData.img = actorImage;

	if (actorSize)
		updateData["system.traits.size"] = actorSize;

	actor.update(updateData).then(() => {
		if (oldForm)
			actor.items.get(oldForm).update({"system.active": false});
	});

	if (tokenImage) tokenData.img = tokenImage;
	if (tokenSize) {
		tokenData.width = tokenSize;
		tokenData.height = tokenSize;
	}

	if (Object.keys(tokenData).length > 0) {
		actor.token.update(tokenData);
		actor.getActiveTokens().forEach((t) => t.document.update(tokenData));
	}
}

function getControlledActors({limit, defaultActors=[], enforceLimit=true, warn=true}={}) {
	let actors = canvas.tokens.controlled.map(token => token.actor);
	if (!actors.length && defaultActors.length)
		actors = game.actors.filter(actor => defaultActors.includes(actor.name));
	if (!actors.length && game.user.character)
		return game.user.character;
	actors = actors.filter(actor => actor.testUserPermission(game.user, "OWNER"));
	if (limit > 0 && actors.length > limit) {
		if (enforceLimit) {
			if (warn) ui.notifications.warn(`Do not select more than ${limit} owned actor${limit > 1 ? "s" : ""}.`);
			throw new Error("Owned actors selected exceeded limit");
		}
		return actors.slice(0, limit);
	}
	if (!actors.length) {
		if (warn) ui.notifications.warn("You must select at least one owned actor.")
			throw new Error("Not enough owned actors selected.");
	}
	return actors;
}

async function selectAndUseAbility(item, abilities, {forceCL, forceSpellbook, itemModifier, dialogContents="", skipDialog=false}={}) {
	let items = [], selectors = [];
	let itemCharges = item.data.data.uses.value;
	for (let i = 0; i < abilities.length; i++) {
		let ability = abilities[i]
		let abilityItem = await fetchItem(ability.id, ability.source);
		if (!abilityItem)
			continue;
		let chargesLabel = ability.charges ? `${ability.charges} ${ability.charges > 1 ? "charges" : "charge"}` : "At will";
		let id = i;
		let disabled = ability.charges > itemCharges;
		items.push(abilityItem);
		selectors.push(renderItemSelector(abilityItem, {label: `${ability.name} (${chargesLabel})`, id, disabled}));
	}
	
	new Dialog({
		content: `${dialogContents}<div class="pf1 chat-card">${selectors.join("")}</div>`,
		buttons: {
			cancel: {label: "Cancel"},
			use: {
				label: "Activate",
				callback: (html) => {
					let i = getInputValue("item", html);
					if (!i) throw "You must select an ability.";
					item.update({"data.uses.value": item.data.data.uses.value - abilities[i].charges});
					// Create a temporary item and protect it from updates and from accidentally modifying other items.
					let tempItem = new game.pf1.polymorphism.ItemBasePF(items[i].data, {parent: item.parent});
					tempItem.data._id = null;
					tempItem.update = () => {};
					// Make useable at will.
					tempItem.data.data.atWill = true;
					tempItem.data.data.usesAmmo = false;
					tempItem.data.data.quantity = null;
					tempItem.data.data.uses.per = null;
					// Set parent flag for script use.
					tempItem.data.data.flags.dictionary.parent = item;
					// Force spellbook and/or CL for spells.
					if (tempItem.type == "spell") {
						if (abilities[i].level) tempItem.data.data.level = ablities[i].level;
						if (forceCL) tempItem.data.data.clOffset += forceCL - (tempItem.spellbook?.cl?.total || 0);
						if (forceSpellbook) tempItem.data.data.spellbook = forceSpellbook;
					}
					// Run custom item modifier.
					if (itemModifier) tempItem = itemModifier(tempItem, html);
					tempItem.use({skipDialog});
				}
			}
		},
		default: "use"
	}).render(true);
}

/**
 * Fetch an item from the indicated source, or the item directory if no source is provided.
 * @param id {String} the id of the item to fetch
 * @param source {String} a compendium key, "directory", or null
 * @return {Item || Promise<Item>} an item from the indicated source
 */
function fetchItem(id, source="directory") {
	if (source == "directory")
		return game.items.get(id);
	let pack = game.packs.get(source);
	return pack.getDocument(id);
}

function getInputValue(name, html) {
	let controls = html[0].querySelectorAll(`button, input, object, output, select, textarea`);
	let matchedControl, i;
	for (i = 0; i < controls.length; i++) {
		if (controls[i].name == name) {
			matchedControl = controls[i];
			break;
		}
	}
	if (!matchedControl)
		return undefined;
	if (matchedControl.type == "radio") {
		if (matchedControl.checked)
			return matchedControl.value;
		for (; i < controls.length; i++) {
			let control = controls[i];
			if (control.name == name && control.type == "radio" && control.checked) {
				return control.value;
			}
		}
		return null;
	}
	if (matchedControl.constructor == HTMLObjectElement)
		return matchedControl.data;
	return matchedControl.value;
}

function renderItemSelector(item, {label, id, img, gradient, disabled}={}) {
	var textColour = (getBrightness(item.typeColor) >= 0.5) ? "black" : "white";
	if (label == null) label = item.name;
	if (id == null) id = item.id;
	if (img == null) img = item.img;
	if (gradient == null) gradient = [item.typeColor, item.typeColor2];
	return `
<div${disabled ? "" : " onclick=\"this.querySelector('input').checked = true;\""} class="card-header" style="background: linear-gradient(to right, ${gradient[0]}, ${gradient[1]}); padding: 3px; border: 2px groove #fff; border-style: groove none;${disabled ? " opacity: 0.5;" : ""}">
	<h3 class="item-name flexrow" style="color: ${textColour}; margin: 0;">
		<img src="${img}" style="width: 36px; flex: 0; margin-right: 5px;">
		<span>${label}</span>
		<input type="radio" name="item" value="${id}" style="flex: 0;"${disabled ? " disabled=\"\"" : ""}>
	</h3>
</div>`;
}

function getBrightness(colour) {
	var [r, g, b] = hexToRGB(colorStringToHex(colour)); // values from 0 to 1
	// https://en.wikipedia.org/wiki/YIQ#From_RGB_to_YIQ
	return (0.299*r + 0.587*g + 0.114*b)
}

Hooks.once("init", () => {
	if (!game.PF1Extensions) {
		game.PF1Extensions = Extensions;
	} else {
		let conflictingKeys = [];
		for (let key of Object.keys(Extensions)) {
			if (game.PF1Extensions[key]) {
				conflictingKeys.push(key);
			} else {
				game.PF1Extensions[key] = Extensions[key];
			}
		}
		if (conflictingKeys) {
			console.errors(`Error exposing PF1 extension methods, the following keys already exist: ${conflictingKeys.join(", ")}`);
			ui.notifications.error("Error exposing PF1 extension methods, see console for more details.", {console: false});
		}
	}
});